package main;

public class CustomStack<T> {
    private Object[] array; // Array to store the elements of the stack
    private int size; // Current size of the stack
    private static final int DEFAULT_CAPACITY = 10; // Default capacity of the stack

    public CustomStack() {
        this.array = new Object[DEFAULT_CAPACITY]; // Initialize the array with default capacity
        this.size = 0; // Initialize the size to 0
    }

    public void push(T item) {
        ensureCapacity(); // Ensure that the array has enough capacity to store the new item
        array[size++] = item; // Add the item to the top of the stack
    }

    public T pop() {
        if (isEmpty()) {
            throw new IllegalStateException("Stack is empty"); // Throw an exception if the stack is empty
        }
        T poppedItem = (T) array[--size]; // Get the item at the top of the stack
        array[size] = null; // Set the reference to null to avoid memory leak
        return poppedItem; // Return the popped item
    }

    public T peek() {
        if (isEmpty()) {
            throw new IllegalStateException("Stack is empty"); // Throw an exception if the stack is empty
        }
        return (T) array[size - 1]; // Return the item at the top of the stack without removing it
    }

    public boolean isEmpty() {
        return size == 0; // Check if the stack is empty
    }

    public int size() {
        return size; // Return the current size of the stack
    }

    private void ensureCapacity() {
        if (size == array.length) { // Check if the array is full
            int newCapacity = size * 2; // Double the capacity of the array
            array = copyArray(newCapacity); // Create a new array with the new capacity and copy the elements
        }
    }

    private Object[] copyArray(int newCapacity) {
        Object[] newArray = new Object[newCapacity]; // Create a new array with the specified capacity
        System.arraycopy(array, 0, newArray, 0, size); // Copy the elements from the old array to the new array
        return newArray; // Return the new array
    }
}
