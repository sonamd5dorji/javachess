package main;

import pieces.*;

import javax.swing.*;
import java.awt.*;

import dataStructures.Node;
import dataStructures.chesspiece;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Board extends JPanel {

    public final int tileSize = 85;
    public static final int COLS = 8;
    public static final int ROWS = 8;
    public static final Color CHECK_SQUARE_COLOR = new Color(255, 0, 0, 100);

    public CustomStack<Move> moveStack = new CustomStack<>();
    private boolean isWhitesTurn = true;
    public int moveCount = 0;

    public JLabel turnLabel;
    public Piece selectedPiece;
    private Timer[] timers;
    private static final int TIMER_DELAY = 1000; // 1 second

    private int whitePlayerTime = 300;
    private int blackPlayerTime = 300;
    private int currentPlayerIndex = 0; // Index of the current player

    chesspiece piece = new chesspiece();
    public Input input = new Input(this);

    public CheckScanner checkScanner = new CheckScanner(this);

    public int enPassantTile = -1;

    // Get the tile number based on the column and row
    public int getTileNum(int col, int row) {
        return row * ROWS + col;
    }

    // Constructor
    public Board() {
        this.setPreferredSize(new Dimension(COLS * tileSize, ROWS * tileSize));

        setLayout(new BorderLayout());

        turnLabel = new JLabel("White's Turn");
        turnLabel.setHorizontalAlignment(SwingConstants.CENTER);
        turnLabel.setFont(new Font(turnLabel.getFont().getName(), Font.PLAIN, 24));
        add(turnLabel, BorderLayout.CENTER);

        timers = new Timer[2];
        for (int i = 0; i < timers.length; i++) {
            int playerIndex = i;
            // Inside the constructor where you set up the timers
            timers[i] = new Timer(TIMER_DELAY, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // Update the corresponding player's time
                    if (playerIndex == 0 && isWhitesTurn) {
                        whitePlayerTime--;
                    } else if (playerIndex == 1 && !isWhitesTurn) {
                        blackPlayerTime--;
                    }

                    if (whitePlayerTime <= 0 || blackPlayerTime <= 0) {
                        System.out.println("Player " + (playerIndex + 1) + " ran out of time!");
                        timers[playerIndex].stop();
                    }

                    if (playerIndex == currentPlayerIndex) {
                        updateTurnLabel();
                    }
                }
            });
        }
        timers[currentPlayerIndex].start();

        this.addMouseListener(input);
        this.addMouseMotionListener(input);
        addPieces();
    }

    public int getMoveCount() {
        return moveCount;
    }

    public Piece getPiece(int col, int row) {
        Node current = chesspiece.top;
        while (current != null) {
            if (current.data.col == col && current.data.row == row) {
                return current.data;
            }
            current = current.next;
        }
        return null;
    }

    public void makeMove(Move move) {
        if (isValidMove(move) && (move.piece.isWhite == isWhitesTurn)) {
            // Create a copy of the move and add it to the stack
            moveStack.push(move);
            if (move.piece.name.equals("Pawn")) {
                movePawn(move);
            } else if (move.piece.name.equals("King")) {
                moveKing(move);
            }
            move.piece.col = move.newCol;
            move.piece.row = move.newRow;
            move.piece.xPos = move.newCol * tileSize;
            move.piece.yPos = move.newRow * tileSize;
            move.piece.isFirstMove = false;
            capture(move);

            moveCount++;

            isWhitesTurn = !isWhitesTurn;
            currentPlayerIndex = isWhitesTurn ? 0 : 1;

            if (!timers[currentPlayerIndex].isRunning()) {
                timers[currentPlayerIndex].start();
            }

            repaint();
        }
    }

    private void moveKing(Move move) {
        if (Math.abs(move.piece.col - move.newCol) == 2) {
            Piece rook;
            if (move.piece.col < move.newCol) {
                rook = getPiece(7, move.piece.row);
                rook.col = 5;
            } else {
                rook = getPiece(0, move.piece.row);
                rook.col = 3;
            }
            rook.xPos = rook.col * tileSize;
        }
    }

    private void movePawn(Move move) {
        int colorIndex = move.piece.isWhite ? 1 : -1;
        if (getTileNum(move.newCol, move.newRow) == enPassantTile) {
            move.capture = getPiece(move.newCol, move.newRow + colorIndex);
        }
        if (Math.abs(move.piece.row - move.newRow) == 2) {
            enPassantTile = getTileNum(move.newCol, move.newRow + colorIndex);
        } else {
            enPassantTile = -1;
        }

        colorIndex = move.piece.isWhite ? 0 : 7;
        if (move.newRow == colorIndex) {
            promotePawn(move);
        }
    }

    private void promotePawn(Move move) {
        // Remove the pawn
        Node current = chesspiece.top;
        Node prev = null;
        while (current != null) {
            if (current.data == move.piece) {
                if (prev != null) {
                    prev.next = current.next;
                } else {
                    chesspiece.top = current.next;
                }
                break;
            }
            prev = current;
            current = current.next;
        }

        // Add the queen
        piece.addPiece(new Queen(this, move.newCol, move.newRow, move.piece.isWhite));
        capture(move);
    }

    public void capture(Move move) {
        Node current = chesspiece.top;
        Node prev = null;
        while (current != null) {
            if (current.data == move.capture) {
                if (prev != null) {
                    prev.next = current.next;
                } else {
                    chesspiece.top = current.next;
                }
                break;
            }
            prev = current;
            current = current.next;
        }
    }

    public boolean isValidMove(Move move) {
        if (sameTeam(move.piece, move.capture)) {
            return false;
        }

        if (!move.piece.isValidMovement(move.newCol, move.newRow)) {
            return false;
        }

        if (move.piece.moveCollidesWithPiece(move.newCol, move.newRow)) {
            return false;
        }

        // Simulate the move to check if the king is in check after the move
        Move simulatedMove = new Move(this, move.piece, move.newCol, move.newRow);
        if (checkScanner.isKingChecked(simulatedMove)) {
            return false;
        }

        // Check if the current player's king is in check
        boolean isKingInCheck = checkScanner.isKingChecked(move);

        // Allow the king to move only if it doesn't result in check
        if (move.piece instanceof King && isKingInCheck) {
            return false;
        }

        return true;
    }


    public boolean sameTeam(Piece p1, Piece p2) {
        if (p1 == null || p2 == null) {
            return false;
        }
        return p1.isWhite == p2.isWhite;
    }

    public Piece findKing(boolean isWhite) {
        Node current = chesspiece.top;

        while (current != null) {
            if (current.data instanceof King && current.data.isWhite == isWhite) {
                return current.data;
            }
            current = current.next;
        }

        return null;
    }

    public void addPieces() {
        piece.addPiece(new Rook(this, 0, 0, false));
        piece.addPiece(new Knight(this, 1, 0, false));
        piece.addPiece(new Bishop(this, 2, 0, false));
        piece.addPiece(new Queen(this, 3, 0, false));
        piece.addPiece(new King(this, 4, 0, false));
        piece.addPiece(new Bishop(this, 5, 0, false));
        piece.addPiece(new Knight(this, 6, 0, false));
        piece.addPiece(new Rook(this, 7, 0, false));

        piece.addPiece(new Pawn(this, 0, 1, false));
        piece.addPiece(new Pawn(this, 1, 1, false));
        piece.addPiece(new Pawn(this, 2, 1, false));
        piece.addPiece(new Pawn(this, 3, 1, false));
        piece.addPiece(new Pawn(this, 4, 1, false));
        piece.addPiece(new Pawn(this, 5, 1, false));
        piece.addPiece(new Pawn(this, 6, 1, false));
        piece.addPiece(new Pawn(this, 7, 1, false));

        piece.addPiece(new Rook(this, 0, 7, true));
        piece.addPiece(new Knight(this, 1, 7, true));
        piece.addPiece(new Bishop(this, 2, 7, true));
        piece.addPiece(new Queen(this, 3, 7, true));
        piece.addPiece(new King(this, 4, 7, true));
        piece.addPiece(new Bishop(this, 5, 7, true));
        piece.addPiece(new Knight(this, 6, 7, true));
        piece.addPiece(new Rook(this, 7, 7, true));

        piece.addPiece(new Pawn(this, 0, 6, true));
        piece.addPiece(new Pawn(this, 1, 6, true));
        piece.addPiece(new Pawn(this, 2, 6, true));
        piece.addPiece(new Pawn(this, 3, 6, true));
        piece.addPiece(new Pawn(this, 4, 6, true));
        piece.addPiece(new Pawn(this, 5, 6, true));
        piece.addPiece(new Pawn(this, 6, 6, true));
        piece.addPiece(new Pawn(this, 7, 6, true));
    }

    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        for (int r = 0; r < ROWS; r++) {
            for (int c = 0; c < COLS; c++) {
                g2d.setColor((c + r) % 2 == 0 ? new Color(227, 198, 181) : new Color(157, 105, 53));
                g2d.fillRect(c * tileSize, r * tileSize, tileSize, tileSize);

                Piece king = findKing(isWhitesTurn);
                if (king != null && king.isInCheck && king.col == c && king.row == r) {
                    g2d.setColor(CHECK_SQUARE_COLOR);
                    g2d.fillRect(c * tileSize, r * tileSize, tileSize, tileSize);
                }
            }
        }

        if (selectedPiece != null) {
            for (int r = 0; r < ROWS; r++) {
                for (int c = 0; c < COLS; c++) {
                    if (isValidMove(new Move(this, selectedPiece, c, r))) {
                        g2d.setColor(new Color(68, 180, 57, 190));
                        g2d.fillRect(c * tileSize, r * tileSize, tileSize, tileSize);
                    }
                }
            }
        }

        Node current = chesspiece.top;
        while (current != null) {
            if (!current.data.isWhite) {
                current.data.paint(g2d);
            }
            current = current.next;
        }

        current = chesspiece.top;
        while (current != null) {
            if (current.data.isWhite) {
                current.data.paint(g2d);
            }
            current = current.next;
        }

        g.setColor(Color.BLACK);
        g.setFont(new Font(g.getFont().getName(), Font.PLAIN, 18));

        String currentPlayerTimeText = isWhitesTurn ? "White's Turn" : "Black's Turn";
        String currentPlayerLabelText = currentPlayerTimeText;


        FontMetrics fontMetrics = g.getFontMetrics();
        int labelWidth = fontMetrics.stringWidth(currentPlayerLabelText);

        int labelX = 40;
        int labelY = isWhitesTurn ? 450 : 250;

        g.setColor(Color.BLACK);
        g.drawString(currentPlayerLabelText, labelX, labelY);
    }

    public boolean isWhitesTurn() {
        return isWhitesTurn;
    }

    public void toggleTurn() {
        isWhitesTurn = !isWhitesTurn;
    }

    private String formatTime(int timeInSeconds) {
        int minutes = timeInSeconds / 60;
        int seconds = timeInSeconds % 60;
        return String.format("%02d:%02d", minutes, seconds);
    }

    private void updateTurnLabel() {
        SwingUtilities.invokeLater(() -> {
            String currentPlayerLabelText = moveStack.size() % 2 == 0 ? "White's Turn" : "Black's Turn";
          
            int currentPlayerTime = isWhitesTurn ? whitePlayerTime : blackPlayerTime;
            turnLabel.setText(currentPlayerLabelText + " - Time: " + formatTime(currentPlayerTime));
        });
    }
}
