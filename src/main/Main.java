package main;

import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args) {

        // Create a new JFrame object
        JFrame frame = new JFrame();

        // Set the background color of the JFrame to black
        frame.getContentPane().setBackground(Color.BLACK);

        // Set the layout of the JFrame to GridBagLayout
        frame.setLayout(new GridBagLayout());

        // Set the minimum size of the JFrame to 1000x1000 pixels
        frame.setMinimumSize((new Dimension(1000,1000)));

        // Center the JFrame on the screen
        frame.setLocationRelativeTo(null);

        // Create a new Board object
        Board board = new Board();

        // Add the Board object to the JFrame
        frame.add(board);

        // Make the JFrame visible
        frame.setVisible(true);
    }
}
