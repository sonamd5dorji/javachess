package main;

import pieces.Piece;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;

public class Input extends MouseAdapter {
    Board board;

    public Input(Board board) {
        this.board = board;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        int col = e.getX() / board.tileSize;
        int row = e.getY() / board.tileSize;
        Piece pieceXY = board.getPiece(col, row);

        // Check if the clicked piece is of the correct color and it's the correct player's turn
        if (pieceXY != null && pieceXY.isWhite == board.isWhitesTurn() && board.getMoveCount() % 2 == (pieceXY.isWhite ? 0 : 1)) {
            board.selectedPiece = pieceXY; // Set the selected piece as the clicked piece
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (board.selectedPiece != null) {
            // Update the position of the selected piece based on the mouse drag
            board.selectedPiece.xPos = e.getX() - board.tileSize / 2;
            board.selectedPiece.yPos = e.getY() - board.tileSize / 2;
        }
        board.repaint(); // Repaint the board to reflect the updated position
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        int col = e.getX() / board.tileSize;
        int row = e.getY() / board.tileSize;
        if (board.selectedPiece != null) {
            Move move = new Move(board, board.selectedPiece, col, row);
            if (!board.isValidMove(move)) {
                // If the move is not valid, reset the position of the selected piece to its original position
                board.selectedPiece.xPos = board.selectedPiece.col * board.tileSize;
                board.selectedPiece.yPos = board.selectedPiece.row * board.tileSize;
            } else {
                board.makeMove(move); // Make the valid move on the board
            }
        }
        board.selectedPiece = null; // Deselect the piece
        board.repaint(); // Repaint the board to reflect the changes
    }
}
