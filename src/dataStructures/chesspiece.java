package dataStructures;

import pieces.Piece;

public class chesspiece {

    public static Node top;


    public void addPiece(Piece piece) {
        Node newNode = new Node(piece);
        newNode.next = top;
        top = newNode;
    }

    // Additional methods if needed
    public static Piece popPiece() {
        if (top != null) {
            Piece popped = top.data;
            top = top.next;
            return popped;
        }
        return null;
    }

    public static Piece peekPiece() {
        if (top != null) {
            return top.data;
        }
        return null;
    }

    public static boolean isEmpty() {
        return top == null;
    }

    public static int size() {
        int count = 0;
        Node current = top;
        while (current != null) {
            count++;
            current = current.next;
        }
        return count;
    }
}
