package dataStructures;
import pieces.Piece;
public class Node {
    public Piece data;
    public Node next;

    public Node(Piece data) {
        this.data = data;
    }
}
