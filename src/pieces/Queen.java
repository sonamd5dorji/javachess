package pieces;

import main.Board;

import java.awt.image.BufferedImage;

public class Queen extends Piece{

    public Queen(Board board, int col, int row, boolean isWhite) {
        super(board);
        this.col=col;
        this.row=row;
        this.xPos=col*board.tileSize;
        this.yPos=row*board.tileSize;
        this.isWhite=isWhite;
        this.name="Queen";
        this.sprite=sheet.getSubimage(sheetScale,isWhite?0:sheetScale,sheetScale,sheetScale).getScaledInstance(board.tileSize,board.tileSize, BufferedImage.SCALE_SMOOTH);
    }

    // Comment: This method checks if the movement of the Queen is valid.
    public boolean isValidMovement(int col, int row) {
        return this.col==col||this.row==row ||  Math.abs(this.col-col)==Math.abs(this.row-row);
    }

    // Comment: This method checks if the Queen's move collides with any other piece on the board.
    public boolean moveCollidesWithPiece(int col, int row){
        if (this.col==col||this.row==row) {
            // Comment: Check for collisions in the left direction.
            if (this.col > col)
                for (int c = this.col - 1; c > col; c--)
                    if (board.getPiece(c, this.row) != null)
                        return true;
            // Comment: Check for collisions in the right direction.
            if (this.col < col)
                for (int c = this.col + 1; c < col; c++)
                    if (board.getPiece(c, this.row) != null)
                        return true;
            // Comment: Check for collisions in the up direction.
            if (this.row > row)
                for (int r = this.row - 1; r > row; r--)
                    if (board.getPiece(this.col, r) != null)
                        return true;
            // Comment: Check for collisions in the down direction.
            if (this.row < row)
                for (int r = this.row + 1; r < row; r++)
                    if (board.getPiece(this.col, r) != null)
                        return true;
        }else {
            // Comment: Check for collisions in the up left direction.
            if(this.col>col && this.row>row)
                for (int i =1;i<Math.abs(this.col-col);i++)
                    if(board.getPiece(this.col-i,this.row-i)!=null)
                        return true;
            // Comment: Check for collisions in the up right direction.
            if(this.col<col && this.row>row)
                for (int i =1;i<Math.abs(this.col-col);i++)
                    if(board.getPiece(this.col+i,this.row-i)!=null)
                        return true;
            // Comment: Check for collisions in the down left direction.
            if(this.col>col && this.row<row)
                for (int i =1;i<Math.abs(this.col-col);i++)
                    if(board.getPiece(this.col-i,this.row+i)!=null)
                        return true;
            // Comment: Check for collisions in the down right direction.
            if(this.col<col && this.row<row)
                for (int i =1;i<Math.abs(this.col-col);i++)
                    if(board.getPiece(this.col+i,this.row+i)!=null)
                        return true;

        }
        return false;

    }
}
